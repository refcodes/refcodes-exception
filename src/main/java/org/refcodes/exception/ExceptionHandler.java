// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

/**
 * The {@link ExceptionHandler} interface provides means to handle exceptions
 * which usually are occurring inside daemon threads and are usually hidden from
 * the (business) logic. Such {@link Exception} instances usually just get
 * logged, no customizable reaction upon such exceptions is possible. This
 * interface enables to process to such {@link Exception} instances by providing
 * a custom implementation if this interface.
 *
 * @param <EXC> the {@link Throwable} type
 */
@FunctionalInterface
public interface ExceptionHandler<EXC extends Throwable> {

	/**
	 * This method is to be overwritten by a custom implementation and is
	 * invoked by a daemon thread upon a caught {@link Exception}.
	 * 
	 * @param aException The exception to be handled.
	 */
	void onException( EXC aException );

}
