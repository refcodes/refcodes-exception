// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception handler applied; as being applied by
// the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

/**
 * Provides an accessor for an {@link ExceptionHandler} property.
 *
 * @param <EXC> The {@link Exception} being handled by the
 *        {@link ExceptionHandler}.
 */
public interface ExceptionHandlerAccessor<EXC extends Throwable> {

	/**
	 * Retrieves the exception handler from the exception handler property.
	 * 
	 * @return The exception handler stored by the exception handler property.
	 */
	ExceptionHandler<EXC> getExceptionHandler();

	/**
	 * Provides a mutator for an {@link ExceptionHandler} property.
	 *
	 * @param <EXC> The {@link Exception} being handled by the
	 *        {@link ExceptionHandler}.
	 */
	public interface ExceptionHandlerMutator<EXC extends Throwable> {

		/**
		 * Sets the exception handler for the exception handler property.
		 * 
		 * @param aExceptionHandler The exception handler to be stored by the
		 *        exception handler property.
		 */
		void setExceptionHandler( ExceptionHandler<EXC> aExceptionHandler );
	}

	/**
	 * Provides a builder method for a exception handler property returning the
	 * builder for applying multiple build operations.
	 *
	 * @param <EXC> The {@link Exception} being handled by the
	 *        {@link ExceptionHandler}.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ExceptionHandlerBuilder<EXC extends Throwable, B extends ExceptionHandlerBuilder<EXC, B>> {

		/**
		 * Sets the exception handler for the exception handler property.
		 * 
		 * @param aExceptionHandler The exception handler to be stored by the
		 *        exception handler property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withExceptionHandler( ExceptionHandler<EXC> aExceptionHandler );
	}

	/**
	 * Provides a an {@link ExceptionHandler} property.
	 *
	 * @param <EXC> The {@link Exception} being handled by the
	 *        {@link ExceptionHandler}.
	 */
	public interface ExceptionHandlerProperty<EXC extends Throwable> extends ExceptionHandlerAccessor<EXC>, ExceptionHandlerMutator<EXC> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ExceptionHandler}
		 * (setter) as of {@link #setExceptionHandler(ExceptionHandler)} and
		 * returns the very same value (getter).
		 * 
		 * @param aExceptionHandler The {@link ExceptionHandler} to set (via
		 *        {@link #setExceptionHandler(ExceptionHandler)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ExceptionHandler<EXC> letExceptionHandler( ExceptionHandler<EXC> aExceptionHandler ) {
			setExceptionHandler( aExceptionHandler );
			return aExceptionHandler;
		}
	}
}
