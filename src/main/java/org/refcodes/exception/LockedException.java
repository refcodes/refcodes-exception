// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

import java.io.Serializable;

/**
 * Thrown in case an instance being locked is still being accessed in a
 * modifying way (see <code>org.refcodes.mixin.Lockable</code> in the
 * refcodes-mixin artifact).
 *
 * @see LockedRuntimeException as the unchecked counterpart of this
 *      {@link Exception}.
 */
public class LockedException extends AbstractException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public LockedException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public LockedException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public LockedException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public LockedException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public LockedException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public LockedException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * Thrown in case an instance being locked is still being accessed in a
	 * modifying way (see <code>org.refcodes.mixin.Lockable</code> in the
	 * refcodes-mixin artifact).
	 * 
	 * @see LockedException as the checked counterpart of this {@link Exception}
	 *      .
	 */
	public static class LockedRuntimeException extends AbstractRuntimeException implements Serializable {

		private static final long serialVersionUID = 1L;

		/**
		 * {@inheritDoc}
		 */
		public LockedRuntimeException( String aMessage, String aErrorCode ) {
			super( aMessage, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public LockedRuntimeException( String aMessage, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public LockedRuntimeException( String aMessage, Throwable aCause ) {
			super( aMessage, aCause );
		}

		/**
		 * {@inheritDoc}
		 */
		public LockedRuntimeException( String aMessage ) {
			super( aMessage );
		}

		/**
		 * {@inheritDoc}
		 */
		public LockedRuntimeException( Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public LockedRuntimeException( Throwable aCause ) {
			super( aCause );
		}
	}
}
