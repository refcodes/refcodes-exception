// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

/**
 * An {@link UnhandledValueBugException} is thrown in case a value was
 * encountered which is not being considered by the code (e.g. in a
 * switch/case-statement): We have a coding error!
 */
public class UnhandledValueBugException extends BugException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Object _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aValue The value not being considered by the code.
	 */
	public UnhandledValueBugException( Object aValue ) {
		super( "Unhandled value <" + aValue + "> of type <" + ( aValue != null ? aValue.getClass() : "null" ) + "> encountered (coding error)!" );
		_value = aValue;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aValue The value not being considered by the code.
	 */
	public UnhandledValueBugException( Object aValue, String aMessage ) {
		super( aMessage );
		_value = aValue;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aValue The value not being considered by the code.
	 */
	public UnhandledValueBugException( Object aValue, String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
		_value = aValue;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aValue The value not being considered by the code.
	 */
	public UnhandledValueBugException( Object aValue, Throwable aCause ) {
		super( aCause );
		_value = aValue;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aValue The value not being considered by the code.
	 */
	public UnhandledValueBugException( Object aValue, String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_value = aValue;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aValue The value not being considered by the code.
	 */
	public UnhandledValueBugException( Object aValue, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_value = aValue;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aValue The value not being considered by the code.
	 */
	public UnhandledValueBugException( Object aValue, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_value = aValue;
	}

	// --------------------------------------------------------------------------
	// METHODS:
	// --------------------------------------------------------------------------

	/**
	 * Returns the unconsidered value in question.
	 * 
	 * @return The according value.
	 */
	public Object getValue() {
		return _value;
	}
}
