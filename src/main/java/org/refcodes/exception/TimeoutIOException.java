// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

/**
 * Thrown in case opening or accessing an open line (connection, junction, link)
 * caused timeout problems.
 */
public class TimeoutIOException extends AbstractIOException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final long _timeoutInMs;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aTimeoutMillis the timeout in ms
	 * @param aMessage The aMessage describing this exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public TimeoutIOException( long aTimeoutMillis, String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_timeoutInMs = aTimeoutMillis;
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aTimeoutMillis the timeout in ms
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public TimeoutIOException( long aTimeoutMillis, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_timeoutInMs = aTimeoutMillis;
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aTimeoutMillis the timeout in ms
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public TimeoutIOException( long aTimeoutMillis, String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
		_timeoutInMs = aTimeoutMillis;
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aTimeoutMillis the timeout in ms
	 * @param aMessage The aMessage describing this exception.
	 */
	public TimeoutIOException( long aTimeoutMillis, String aMessage ) {
		super( aMessage );
		_timeoutInMs = aTimeoutMillis;
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aTimeoutMillis the timeout in ms
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 * @param aErrorCode The error code identifying this exception.
	 */
	public TimeoutIOException( long aTimeoutMillis, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_timeoutInMs = aTimeoutMillis;
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aTimeoutMillis the timeout in ms
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public TimeoutIOException( long aTimeoutMillis, Throwable aCause ) {
		super( aCause );
		_timeoutInMs = aTimeoutMillis;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The timeout attribute in milliseconds.
	 * 
	 * @return An long integer with the timeout in milliseconds.
	 */
	public long getTimeoutInMs() {
		return _timeoutInMs;
	}
}
