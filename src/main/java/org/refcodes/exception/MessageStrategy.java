// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

/**
 * Produces a {@link #getMessage(Throwable)} message when called upon the
 * {@link #MESSAGE} enumeration or a localized message when called upon the
 * {@link #LOCALIZED_MESSAGE} enumeration from a provided {@link Throwable}
 * instance (strategy pattern).
 */
public enum MessageStrategy {

	/**
	 * Used to produce exception messages from {@link Exception} instances (as
	 * of the {@link Throwable#getMessage()} method).
	 */
	MESSAGE {
		@Override
		String getMessage( Throwable aException ) {
			return aException.getMessage();
		}
	},

	/**
	 * Used to produce localized exception messages from {@link Exception}
	 * instances (as of the {@link Throwable#getLocalizedMessage()} method).
	 */
	LOCALIZED_MESSAGE {
		@Override
		String getMessage( Throwable aException ) {
			return aException.getLocalizedMessage();
		}
	};

	/**
	 * Retrieves the according message {@link #MESSAGE} or {link
	 * #LOCALIZED_MESSAGE} from an {@link Exception}.
	 * 
	 * @param aException The {@link Throwable} ({@link Exception}) from which to
	 *        retrieve the (localized) message.
	 * 
	 * @return The (localized) message.
	 */
	abstract String getMessage( Throwable aException );
}
