// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

/**
 * The {@link MessageDetails} enumeration provides different detail levels when
 * generating exception messages from {@link Throwable} exceptions using the
 * {@link Trap#toMessage(MessageDetails)} method. The {@link MessageDetails}
 * enumeration actually defines {@link Function} methods for construction the
 * messages and the {@link Trap#toMessage(MessageDetails)} method actually just
 * forwards its {@link MessageDetails} argument (itself being a
 * {@link Function}) to the {@link Trap#toMessage(Function)} method.
 */
public enum MessageDetails implements Function<Throwable, String> {

	/**
	 * Same as {@link Throwable#getMessage()}
	 */
	PLAIN(e -> e.getMessage()),

	/**
	 * Same as {@link Throwable#getLocalizedMessage()}
	 */
	PLAIN_LOCALIZED(e -> e.getLocalizedMessage()),

	/**
	 * Just the messages from the root causes (the roots of
	 * {@link Throwable#getCause()}) and root suppressed exceptions (the roots
	 * of {@link Throwable#getSuppressed()}) are taken into account.
	 */
	SHORT(e -> toMessage( e, true, MessageStrategy.MESSAGE )),

	/**
	 * Just the localized messages from the root causes (the roots of
	 * {@link Throwable#getCause()}) and root suppressed exceptions (the roots
	 * of {@link Throwable#getSuppressed()}) are taken into account.
	 */
	SHORT_LOCALIZED(e -> toMessage( e, true, MessageStrategy.LOCALIZED_MESSAGE )),

	/**
	 * All messages including all (children's) causes
	 * ({@link Throwable#getCause()}) together with all (children's) suppressed
	 * exceptions ({@link Throwable#getSuppressed()}) are taken into account.
	 */
	FULL(e -> toMessage( e, false, MessageStrategy.MESSAGE )),

	/**
	 * All localized messages including all (children's) causes
	 * ({@link Throwable#getCause()}) together with all (children's) suppressed
	 * exceptions ({@link Throwable#getSuppressed()}) are taken into account.
	 */
	FULL_LOCALIZED(e -> toMessage( e, false, MessageStrategy.LOCALIZED_MESSAGE ));

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String CAUSED_BY = "Caused by";
	public static final String ALSO_NOTABLE = "Also notable";
	public static final String POSSIBLE_CAUSE = "Possible cause";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Function<Throwable, String> _function;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private MessageDetails( Function<Throwable, String> aFunction ) {
		_function = aFunction;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String apply( Throwable eException ) {
		return _function.apply( eException );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static String toMessage( Throwable aException, boolean isLeafsOnly, MessageStrategy aMessageStrategy ) {
		return toMessage( aException, isLeafsOnly, aMessageStrategy, new HashSet<>() );
	}

	private static String toMessage( Throwable aException, boolean isLeafsOnly, MessageStrategy aMessageStrategy, Set<Throwable> aVistedExceptions ) {
		if ( aVistedExceptions.contains( aException ) ) {
			return "";
		}
		aVistedExceptions.add( aException );

		String theMessage = "";
		if ( !isLeafsOnly || ( aException.getCause() == null || aException.getCause() == aException ) ) {
			theMessage = toNormalizedMessage( aMessageStrategy.getMessage( aException ) );
		}
		if ( aException.getSuppressed() != null && aException.getSuppressed().length != 0 ) {
			if ( theMessage.length() != 0 ) {
				theMessage += " " + ALSO_NOTABLE + ": ";
			}
			boolean isFirst = true;
			for ( Throwable e : aException.getSuppressed() ) {
				if ( isFirst ) {
					isFirst = false;
				}
				else {
					if ( theMessage.length() != 0 ) {
						theMessage += " ";
					}
				}
				theMessage += toMessage( e, isLeafsOnly, aMessageStrategy, aVistedExceptions );
			}
		}
		if ( aException.getCause() != null && aException.getCause() != aException ) {
			if ( theMessage.length() != 0 ) {
				theMessage += " " + CAUSED_BY + ": ";
			}
			theMessage += toMessage( aException.getCause(), isLeafsOnly, aMessageStrategy, aVistedExceptions );
		}
		return theMessage != null && theMessage.length() != 0 ? theMessage : aMessageStrategy.getMessage( aException );
	}

	private static String toNormalizedMessage( String aMessage ) {
		if ( aMessage == null ) {
			aMessage = "";
		}
		aMessage = aMessage.trim();
		if ( aMessage.length() > 0 ) {
			if ( Character.isLowerCase( aMessage.charAt( 0 ) ) ) {
				aMessage = Character.toUpperCase( aMessage.charAt( 0 ) ) + aMessage.substring( 1 );
			}
			if ( !isEndOfSentence( aMessage.charAt( aMessage.length() - 1 ) ) ) {
				aMessage = aMessage += '.';
			}
		}
		return aMessage;
	}

	private static boolean isEndOfSentence( char aChar ) {
		return switch ( aChar ) {
		case '?' -> true;
		case '!' -> true;
		case '.' -> true;
		default -> false;
		};
	}
}
