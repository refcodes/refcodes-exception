// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

/**
 * Provides an accessor for an {@link Exception} property.
 *
 * @param <EXC> The actual exception type.
 */
public interface ExceptionAccessor<EXC extends Throwable> {

	/**
	 * Retrieves the exception from the exception property.
	 * 
	 * @return The exception stored by the exception property.
	 */
	EXC getException();

	/**
	 * Provides a mutator for an {@link Exception} property.
	 *
	 * @param <EXC> The actual exception type.
	 */
	public interface ExceptionMutator<EXC extends Throwable> {

		/**
		 * Sets the exception for the exception property.
		 * 
		 * @param aException The exception to be stored by the exception
		 *        property.
		 */
		void setException( EXC aException );
	}

	/**
	 * Provides a builder method for a exception property returning the builder
	 * for applying multiple build operations.
	 *
	 * @param <EXC> The actual exception type.
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ExceptionBuilder<EXC extends Throwable, B extends ExceptionBuilder<EXC, B>> {

		/**
		 * Sets the exception for the exception property.
		 * 
		 * @param aException The exception to be stored by the exception
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withException( EXC aException );
	}

	/**
	 * Provides a an {@link Exception} property.
	 *
	 * @param <EXC> The actual exception type.
	 */
	public interface ExceptionProperty<EXC extends Throwable> extends ExceptionAccessor<EXC>, ExceptionMutator<EXC> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Exception}
		 * (setter) as of {@link #setException(Throwable)} and returns the very
		 * same value (getter).
		 * 
		 * @param aException The {@link Exception} to set (via
		 *        {@link #setException(Throwable)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default EXC letException( EXC aException ) {
			setException( aException );
			return aException;
		}
	}
}
