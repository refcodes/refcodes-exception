// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

import javax.security.auth.Destroyable;

/**
 * The {@link Exceptional} interface provides means for threads to wait (sleep)
 * when calling {@link #catchException()} till a next {@link Exception} occurs
 * or till the waiting (sleeping) threads are released.
 * <p>
 * Usually only {@link Exception} instances are exposed which are occurring
 * inside daemon threads and are usually hidden from the (business) logic. Such
 * {@link Exception} instances usually just get logged, no customizable reaction
 * upon such exceptions is possible. This interface enables to react to such
 * {@link Exception} instances without the need to overwrite an abstract class's
 * method and without requiring the implementation of an observable interface.
 * <p>
 * It depends on the implementation on how waiting (sleeping) threads are
 * released, this can be established for example upon disposal by invoking
 * <code>Disposable#dispose()</code> or destruction by invoking
 * <code>Destroyable#destroy()</code>.
 *
 * @param <EXC> the {@link Throwable} type
 */
public interface Exceptional<EXC extends Throwable> {

	/**
	 * This methods awaits an {@link Exception}, usually happening inside a
	 * daemon thread and therefore not directly accessible by some (business)
	 * logic. It waits (sleeps) until an {@link Exception} arises which is then
	 * thrown by this method. Upon disposal (<code>Disposable#dispose()</code>)
	 * or destruction ( {@link Destroyable#destroy()}) all waiting threads are
	 * continued without throwing any exception.
	 * -------------------------------------------------------------------------
	 * ATTENTION: In case waiting (sleeping) threads are released without
	 * throwing an {@link Exception}, then this means that the implementing
	 * instance intends to shut down so that in such as case, no more calls to
	 * this method are to be performed (are to be prevented), such looping
	 * threads would lead to endless looping and aCause consumption of
	 * calculation power. Implementing instances might throw an
	 * {@link IllegalStateException} exception in case such a status is
	 * detected!
	 * 
	 * @throws EXC Thrown when a next exception has been provided via
	 *         <code>ExceptionWatchdog#throwException(Exception)</code>
	 * @throws IllegalStateException Thrown in case this method is being called
	 *         even though the implementing instance has been shut down and has
	 *         released all threads without throwing an exception.
	 */
	void catchException() throws EXC;

	/**
	 * Returns the last exception which was catchable via
	 * {@link #catchException()}.
	 * 
	 * @return The last exception being catchable via {@link #catchException()};
	 */
	EXC lastException();

}
