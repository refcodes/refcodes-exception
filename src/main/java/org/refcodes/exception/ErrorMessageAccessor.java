// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

/**
 * Provides an accessor for a error aMessage property.
 */
public interface ErrorMessageAccessor {

	/**
	 * Retrieves the error aMessage from the error aMessage property.
	 * 
	 * @return The error aMessage stored by the error aMessage property.
	 */
	String getErrorMessage();

	/**
	 * Provides a mutator for a error aMessage property.
	 */
	public interface ErrorMessageMutator {

		/**
		 * Sets the error aMessage for the error aMessage property.
		 * 
		 * @param aErrorMessage The error aMessage to be stored by the error
		 *        aMessage property.
		 */
		void setErrorMessage( String aErrorMessage );
	}

	/**
	 * Provides a builder method for a error aMessage property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ErrorMessageBuilder<B extends ErrorMessageBuilder<B>> {

		/**
		 * Sets the error aMessage for the error aMessage property.
		 * 
		 * @param aErrorMessage The error aMessage to be stored by the error
		 *        aMessage property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withErrorMessage( String aErrorMessage );
	}

	/**
	 * Provides a error aMessage property.
	 */
	public interface ErrorMessageProperty extends ErrorMessageAccessor, ErrorMessageMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given error aMessage (setter)
		 * as of {@link #setErrorMessage(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aErrorMessage The error aMessage to set (via
		 *        {@link #setErrorMessage(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letErrorMessage( String aErrorMessage ) {
			setErrorMessage( aErrorMessage );
			return aErrorMessage;
		}
	}
}
