// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

import java.io.IOException;

/**
 * Unchecked exception with the same semantics as the {@link IOException}.
 */
public class RuntimeIOException extends AbstractRuntimeException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _errorCode = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public RuntimeIOException( String aMessage, String aErrorCode ) {
		super( aMessage );
		_errorCode = aErrorCode;
	}

	/**
	 * {@inheritDoc}
	 */
	public RuntimeIOException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause );
		_errorCode = aErrorCode;
	}

	/**
	 * {@inheritDoc}
	 */
	public RuntimeIOException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public RuntimeIOException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public RuntimeIOException( Throwable aCause, String aErrorCode ) {
		super( aCause );
		_errorCode = aErrorCode;
	}

	/**
	 * {@inheritDoc}
	 */
	public RuntimeIOException( Throwable aCause ) {
		super( aCause );
	}

	// /////////////////////////////////////////////////////////////////////////
	// ATTRIBUTES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getErrorCode() {
		return _errorCode;
	}
}