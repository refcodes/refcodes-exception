// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

/**
 * Provides an accessor for an error code property. An exception code can be
 * provided when creating an exception and it can be used by the business logic
 * to retrieve a language depended exception aMessage (multi-language support)
 * from a language pack instead of just providing the single language aMessage
 * as being stored by the default exceptions.
 */
public interface ErrorCodeAccessor {

	/**
	 * Provides the according exception code. An exception code can be provided
	 * when creating an exception and it can be used by the business logic to
	 * retrieve a language depended exception aMessage (multi-language support)
	 * from a language pack instead of just providing the single language
	 * aMessage as being stored by the default exceptions.
	 * 
	 * @return The exception code in question.
	 */
	String getErrorCode();

	/**
	 * Provides a mutator for an error code property. An exception code can be
	 * provided when creating an exception and it can be used by the business
	 * logic to retrieve a language depended exception aMessage (multi-language
	 * support) from a language pack instead of just providing the single
	 * language aMessage as being stored by the default exceptions.
	 */
	public interface ErrorCodeMutator {

		/**
		 * Sets the according exception code. See {@link #getErrorCode()}.
		 * 
		 * @param aErrorCode The exception code in question.
		 */
		void setErrorCode( String aErrorCode );
	}

	/**
	 * Provides a builder method for a error code property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ErrorCodeBuilder<B extends ErrorCodeBuilder<B>> {

		/**
		 * Sets the error code for the error code property.
		 * 
		 * @param aErrorCode The error code to be stored by the error code
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withErrorCode( String aErrorCode );
	}

	/**
	 * Provides an error code property.
	 */
	public interface ErrorCodeProperty extends ErrorCodeAccessor, ErrorCodeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given error code (setter) as
		 * of {@link #setErrorCode(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aErrorCode The error code to set (via
		 *        {@link #setErrorCode(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letErrorCode( String aErrorCode ) {
			setErrorCode( aErrorCode );
			return aErrorCode;
		}
	}
}
