// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.function.Function;

/**
 * Interface declaring base exception functionality for all of refcodes
 * exceptions.
 */
public interface Trap extends ErrorCodeAccessor {

	/**
	 * Generates a message using the provided {@link MessageDetails} enumeration
	 * defining the message's detail level.
	 * 
	 * @param aMessageDetails The {@link MessageDetails} enumeration defining
	 *        the message's detail level.
	 * 
	 * @return The according produced {@link Exception}'s message.
	 */
	default String toMessage( MessageDetails aMessageDetails ) {
		return toMessage( (Function<Throwable, String>) aMessageDetails );
	}

	/**
	 * Generates a message using the provided {@link Function} method producing
	 * the message.
	 * 
	 * @param aMessageFunction The {@link Function} method producing the
	 *        message.
	 * 
	 * @return The according produced {@link Exception}'s message.
	 */
	default String toMessage( Function<Throwable, String> aMessageFunction ) {
		if ( this instanceof Throwable t ) {
			return aMessageFunction.apply( t );
		}
		throw new UnsupportedOperationException( "The provided type <" + getClass().getName() + "> is not an exception type though this method is only applicable to types extending the <" + Throwable.class.getName() + "> type)!" );
	}

	/**
	 * Generates a message by considering a {@link Throwable}'s cause
	 * {@link Throwable#getCause()} as well as suppressed
	 * {@link Throwable#getSuppressed()} exceptions.
	 * 
	 * @return The according more verbose {@link Exception}'s message.
	 */
	default String toMessage() {
		return ( toMessage( MessageDetails.FULL ) );
	}

	/**
	 * Generates a localized message by considering a {@link Throwable}'s
	 * {@link Throwable#getCause()} cause as well as
	 * {@link Throwable#getSuppressed()} exceptions.
	 * 
	 * @return The according more verbose {@link Exception}'s message.
	 */
	default String toLocalizedMessage() {
		return ( toMessage( MessageDetails.FULL_LOCALIZED ) );
	}

	/**
	 * Generates a short message by just considering a {@link Throwable}'s root
	 * cause {@link Throwable#getCause()} as well as root
	 * {@link Throwable#getSuppressed()} exceptions only.
	 * 
	 * @return The according more verbose shorter {@link Exception}'s message.
	 */
	default String toShortMessage() {
		return ( toMessage( MessageDetails.SHORT ) );
	}

	/**
	 * Generates a short localized message by just considering a
	 * {@link Throwable}'s {@link Throwable#getCause()} root cause as well as
	 * root {@link Throwable#getSuppressed()} exceptions only.
	 * 
	 * @return The according more verbose shorter {@link Exception}'s message.
	 */
	default String toShortLocalizedMessage() {
		return ( toMessage( MessageDetails.SHORT_LOCALIZED ) );
	}

	/**
	 * Provides the arguments for the {@link Throwable#getMessage()} method to
	 * create the actual message from the message pattern (passed as message
	 * argument to the exception's constructors). The message pattern together
	 * with the arguments are passed to the
	 * {@link Trap#asMessage(String, Object...)} method). Override the
	 * {@link #getPatternArguments()} method to provide the required arguments
	 * for applying {@link Trap#asMessage(String, Object...)} to the message
	 * template! Providing <code>null</code> will return the message pattern
	 * untouched.
	 * 
	 * @return The arguments required to build a message from the message
	 *         pattern or null if the message pattern is to be returned
	 *         directly!
	 */
	default Object[] getPatternArguments() {
		return null;
	}

	/**
	 * Generates a message by considering the {@link Throwable}'s cause
	 * {@link Throwable#getCause()} as well as {@link Throwable#getSuppressed()}
	 * exceptions.
	 * 
	 * @param aException The {@link Throwable} from which to produce the
	 *        message.
	 * 
	 * @return The according more verbose {@link Exception}'s message.
	 */
	static String asMessage( Throwable aException ) {
		return MessageDetails.FULL.apply( aException );
	}

	/**
	 * Generates a localized message by considering a {@link Throwable}'s cause
	 * {@link Throwable#getCause()} as well as {@link Throwable#getSuppressed()}
	 * exceptions.
	 * 
	 * @param aException The {@link Throwable} from which to produce the
	 *        message.
	 * 
	 * @return The according more verbose {@link Exception}'s message.
	 */
	static String asLocalizedMessage( Throwable aException ) {
		return MessageDetails.FULL_LOCALIZED.apply( aException );
	}

	/**
	 * Generates a short message by just considering the {@link Throwable}'s
	 * root cause {@link Throwable#getCause()} as well as
	 * {@link Throwable#getSuppressed()} root exceptions only.
	 * 
	 * @param aException The {@link Throwable} from which to produce the
	 *        message.
	 * 
	 * @return The according more verbose {@link Exception}'s message.
	 */
	static String asShortMessage( Throwable aException ) {
		return MessageDetails.SHORT.apply( aException );
	}

	/**
	 * Generates a short localized message by just considering a
	 * {@link Throwable}'s root cause {@link Throwable#getCause()} as well as
	 * {@link Throwable#getSuppressed()} root exceptions only.
	 * 
	 * @param aException The {@link Throwable} from which to produce the
	 *        message.
	 * 
	 * @return The according more verbose {@link Exception}'s message.
	 */
	static String asShortLocalizedMessage( Throwable aException ) {
		return MessageDetails.SHORT_LOCALIZED.apply( aException );
	}

	/**
	 * Creates a message from the provided message and the {@link Exception}'s
	 * contained message(s).
	 *
	 * @param aMessage The message for which to generate the message.
	 * @param aException The {@link Throwable} from which to produce the message
	 *        details.
	 * 
	 * @return The resulting message.
	 */
	static String asMessage( String aMessage, Throwable aException ) {
		return toMessage( aMessage, aException, MessageDetails.FULL );
	}

	/**
	 * Creates a message from the provided message and the {@link Exception}'s
	 * contained localized message(s).
	 *
	 * @param aMessage The message for which to generate the message.
	 * @param aException The {@link Throwable} from which to produce the message
	 *        details.
	 * 
	 * 
	 * @return The resulting message.
	 */
	static String asLocalizedMessage( String aMessage, Throwable aException ) {
		return toMessage( aMessage, aException, MessageDetails.FULL_LOCALIZED );
	}

	/**
	 * Wraps {@link MessageFormat#format(String, Object...)} to prevent failing
	 * when message cannot be formatted. Also expands any arrays passed inside
	 * the arguments to a comma separated {@link String} of the array's elements
	 * as of {@link Arrays#toString(Object[])} without the square braces: In
	 * case such an expanded array's length is 0, then the according placeholder
	 * (such as "{0}", "{1}" and "{2}" and so on) is preserved in the resulting
	 * {@link String}!
	 *
	 * @param aMessage The message to be formatted by substituting the
	 *        placeholder ("{0}", "{1}" and "{2}" and so on) with the according
	 *        arguemtns's elements.
	 * @param aArguments the arguments The arguments which are used for
	 *        substitution.
	 * 
	 * @return The substituted message.
	 */
	static String asMessage( String aMessage, Object... aArguments ) {
		if ( aArguments == null || aArguments.length == 0 ) {
			return aMessage;
		}
		try {
			final Object[] theArguments = new Object[aArguments.length];
			for ( int i = 0; i < aArguments.length; i++ ) {
				theArguments[i] = toString( aArguments[i], i );
			}
			return MessageFormat.format( aMessage, theArguments );
		}
		catch ( IllegalArgumentException e ) {
			if ( aArguments == null || aArguments.length == 0 ) {
				return aMessage;
			}
			final Object[] theArguments = new Object[aArguments.length];
			for ( int i = 0; i < aArguments.length; i++ ) {
				theArguments[i] = toString( aArguments[i], i );
			}
			return aMessage + " (" + toString( theArguments ) + ")";
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static Object toString( Object aObject, int aIndex ) {
		if ( aObject instanceof Object[] aObjects ) {
			final String theString = toString( aObjects );
			return theString.isEmpty() ? "{" + aIndex + "}" : theString;
		}
		// return toString( aObject );
		return aObject;
	}

	private static String toString( Object[] aObjects ) {
		if ( aObjects != null && aObjects.length == 1 ) {
			final Object theString = toString( aObjects[0] );
			return theString instanceof String str ? str : ( theString != null ? theString.toString() : null );
		}
		final Object[] theObjects = new Object[aObjects.length];
		for ( int i = 0; i < theObjects.length; i++ ) {
			theObjects[i] = toString( aObjects[i] );
		}
		final String theString = theObjects.length == 0 ? "" : Arrays.toString( theObjects );
		//	if ( theString != null && theString.length() > 0 && theString.charAt( 0 ) == '[' ) {
		//		theString = theString.substring( 1 );
		//	}
		//	if ( theString != null && theString.length() > 0 && theString.charAt( theString.length() - 1 ) == ']' ) {
		//		theString = theString.substring( 0, theString.length() - 1 );
		//	}
		return theString;
	}

	private static Object toString( Object aObject ) {
		if ( aObject instanceof String theStr ) {
			if ( !theStr.startsWith( "\"" ) && !theStr.endsWith( "\"" ) && !theStr.startsWith( "[\"" ) && !theStr.endsWith( "\"]" ) ) {
				return "\"" + theStr + "\"";
			}
			else {
				return theStr;
			}
		}
		else if ( aObject instanceof Character theChar ) {
			return "'" + theChar + "'";
		}
		//	else if ( aObject != null && !aObject.getClass().isArray() && !( aObject instanceof Number ) && !( aObject instanceof Boolean ) ) {
		//		final String theStr = aObject.toString();
		//		if ( !theStr.startsWith( "\"" ) && !theStr.endsWith( "\"" ) ) {
		//			return "\"" + theStr + "\"";
		//		}
		//		else {
		//			return theStr;
		//		}
		//	}
		return aObject;
	}

	private static String toMessage( String aMessage, Throwable aException, MessageDetails aMessageDetails ) {
		final String theMessage = aMessageDetails.apply( aException );
		if ( theMessage != null && theMessage.length() != 0 ) {
			theMessage.trim();
			if ( theMessage.length() != 0 ) {
				aMessage = aMessage + " " + theMessage;
			}
		}
		return aMessage;
	}
}
