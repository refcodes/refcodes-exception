// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

/**
 * An {@link UnhandledEnumBugException} is thrown in case an enumeration was
 * encountered which is not being considered by the code (e.g. in a
 * switch/case-statement): We have a coding error!
 */
public class UnhandledEnumBugException extends BugException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Enum<?> _enum;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEnum The enumeration not being considered by the code.
	 */
	public UnhandledEnumBugException( Enum<?> aEnum ) {
		super( "Unhandled enumeration <" + aEnum + "> of type <" + ( aEnum != null ? aEnum.getClass() : "null" ) + "> encountered (coding error)!" );
		_enum = aEnum;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEnum The enumeration not being considered by the code.
	 */
	public UnhandledEnumBugException( Enum<?> aEnum, String aMessage ) {
		super( aMessage );
		_enum = aEnum;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEnum The enumeration not being considered by the code.
	 */
	public UnhandledEnumBugException( Enum<?> aEnum, String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
		_enum = aEnum;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEnum The enumeration not being considered by the code.
	 */
	public UnhandledEnumBugException( Enum<?> aEnum, Throwable aCause ) {
		super( aCause );
		_enum = aEnum;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEnum The enumeration not being considered by the code.
	 */
	public UnhandledEnumBugException( Enum<?> aEnum, String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_enum = aEnum;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEnum The enumeration not being considered by the code.
	 */
	public UnhandledEnumBugException( Enum<?> aEnum, String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_enum = aEnum;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aEnum The enumeration not being considered by the code.
	 */
	public UnhandledEnumBugException( Enum<?> aEnum, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_enum = aEnum;
	}

	// --------------------------------------------------------------------------
	// METHODS:
	// --------------------------------------------------------------------------

	/**
	 * Returns the unconsidered enumeration in question.
	 * 
	 * @return The according enumeration.
	 */
	public Enum<?> getEnum() {
		return _enum;
	}
}
