// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

/**
 * Thrown when unmarshaling / deserializing an object fails.
 */
public class UnmarshalException extends AbstractException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected int _offset = -1;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public UnmarshalException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aOffset The offset in the char sequence causing problems.
	 */
	public UnmarshalException( String aMessage, int aOffset, Throwable aCause ) {
		super( aMessage, aCause );
		_offset = aOffset;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aOffset The offset in the char sequence causing problems.
	 */
	public UnmarshalException( String aMessage, int aOffset ) {
		super( aMessage );
		_offset = aOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	public UnmarshalException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnmarshalException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnmarshalException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnmarshalException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnmarshalException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * In case of a char sequence, the offset represents the position at which
	 * unmarshaling failed.
	 * 
	 * @return The position at which unmarshaling failed or -1 if none such
	 *         position is available.
	 */
	public int getOffset() {
		return _offset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getPatternArguments() {
		return new Object[] { _offset };
	}
}
