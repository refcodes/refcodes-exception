// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

/**
 * Base exception for hidden (unchecked) / subsystem exceptions, providing
 * functionality for exception specific multi-language messages. A hidden
 * exception is an exception not representing the behavior of a system by its
 * declared checked and runtime exceptions. Moreover, hidden exceptions may
 * originate from subsystems wrapped by a system without the wrapping system
 * being able to declare the actual subsystem's aCause in its declarations (due
 * to e.g. interface compliance reasons). You as a programmer cannot take care
 * to prevent such exception as them may be even undocumented. Such exceptions
 * may be exceptions wrapping an actual aCause. Requiring a hidden exceptions
 * may point to problems of your software design, the use of hidden exceptrions
 * should be avoided.
 */
public abstract class AbstractHiddenException extends RuntimeException implements Trap {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////

	private String _errorCode = null;

	// /////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public AbstractHiddenException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public AbstractHiddenException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aMessage - the detail aMessage (which is saved for later retrieval
	 *        by the Throwable.getMessage() method).
	 * @param aCause - the aCause (which is saved for later retrieval by the
	 *        Throwable.getCause() method). (A null value is permitted, and
	 *        indicates that the aCause is nonexistent or unknown.)
	 * @param aErrorCode The exception code to be assigned to the exception. An
	 *        exception code can be provided when creating an exception and it
	 *        can be used by the business logic to retrieve a language depended
	 *        exception aMessage (multi-language support) from a language pack
	 *        instead of just providing the single language aMessage as being
	 *        stored by the default exceptions.
	 */
	public AbstractHiddenException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause );
		_errorCode = aErrorCode;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aCause - the aCause (which is saved for later retrieval by the
	 *        Throwable.getCause() method). (A null value is permitted, and
	 *        indicates that the aCause is nonexistent or unknown.)
	 * @param aErrorCode The exception code to be assigned to the exception. An
	 *        exception code can be provided when creating an exception and it
	 *        can be used by the business logic to retrieve a language depended
	 *        exception aMessage (multi-language support) from a language pack
	 *        instead of just providing the single language aMessage as being
	 *        stored by the default exceptions.
	 */
	public AbstractHiddenException( Throwable aCause, String aErrorCode ) {
		super( aCause );
		_errorCode = aErrorCode;
	}

	// /////////////////////////////////////////////////////////////////////////
	// ATTRIBUTES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getErrorCode() {
		return _errorCode;
	}
}