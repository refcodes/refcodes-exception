// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

import java.io.IOException;

/**
 * Base exception for I/O exceptions, providing functionality for exception
 * specific multi-language messages. Unpredictable exceptions must be declared
 * or explicitly caught. You as a programmer cannot take care to prevent such
 * exceptions by according (business) logic. Such exceptions may be network
 * connection loss or external peripheral failure.
 */
public abstract class AbstractIOException extends IOException implements Trap {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _errorCode = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public AbstractIOException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aMessage The aMessage describing this exception.
	 */
	public AbstractIOException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * Instantiates a new according exception.
	 *
	 * @param aCause The {@link Throwable} ({@link Exception}) causing this
	 *        exception.
	 */
	public AbstractIOException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aMessage - the detail aMessage (which is saved for later retrieval
	 *        by the Throwable.getMessage() method).
	 * @param aCause - the aCause (which is saved for later retrieval by the
	 *        Throwable.getCause() method). (A null value is permitted, and
	 *        indicates that the aCause is nonexistent or unknown.)
	 * @param aErrorCode The exception code to be assigned to the exception. An
	 *        exception code can be provided when creating an exception and it
	 *        can be used by the business logic to retrieve a language depended
	 *        exception aMessage (multi-language support) from a language pack
	 *        instead of just providing the single language aMessage as being
	 *        stored by the default exceptions.
	 */
	public AbstractIOException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause );
		_errorCode = aErrorCode;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aMessage - the detail aMessage (which is saved for later retrieval
	 *        by the Throwable.getMessage() method).
	 * @param aErrorCode The exception code to be assigned to the exception. An
	 *        exception code can be provided when creating an exception and it
	 *        can be used by the business logic to retrieve a language depended
	 *        exception aMessage (multi-language support) from a language pack
	 *        instead of just providing the single language aMessage as being
	 *        stored by the default exceptions.
	 */
	public AbstractIOException( String aMessage, String aErrorCode ) {
		super( aMessage );
		_errorCode = aErrorCode;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aCause - the aCause (which is saved for later retrieval by the
	 *        Throwable.getCause() method). (A null value is permitted, and
	 *        indicates that the aCause is nonexistent or unknown.)
	 * @param aErrorCode The exception code to be assigned to the exception. An
	 *        exception code can be provided when creating an exception and it
	 *        can be used by the business logic to retrieve a language depended
	 *        exception aMessage (multi-language support) from a language pack
	 *        instead of just providing the single language aMessage as being
	 *        stored by the default exceptions.
	 */
	public AbstractIOException( Throwable aCause, String aErrorCode ) {
		super( aCause );
		_errorCode = aErrorCode;
	}

	// /////////////////////////////////////////////////////////////////////////
	// ATTRIBUTES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getErrorCode() {
		return _errorCode;
	}
}
