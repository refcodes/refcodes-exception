// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.exception;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class MessageTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testGetMessage() {
		final BugException theException = createException();
		final String theMessage = theException.getMessage();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "getMessage() = " + theMessage );
		}
		assertEquals( "Exception", theMessage );
		assertEquals( "Exception", theException.toMessage( MessageDetails.PLAIN ) );
	}

	@Test
	public void testGetLocalizedMessage() {
		final BugException theException = createException();
		final String theMessage = theException.getLocalizedMessage();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "getLocalizedMessage() = " + theMessage );
		}
		assertEquals( "Exception", theMessage );
		assertEquals( "Exception", theException.toMessage( MessageDetails.PLAIN_LOCALIZED ) );
	}

	@Test
	public void testToMessage() {
		final BugException theException = createException();
		final String theMessage = theException.toMessage();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "toMessage() = " + theMessage );
		}
		assertEquals( "Exception. Also notable: Suppressed 1. Caused by: Cause 1. Suppressed 2. Caused by: Cause A. Also notable: Suppressed A1. Caused by: Cause A1. Suppressed A2. Caused by: Cause B. Also notable: Suppressed B1. Caused by: Cause B1. Suppressed B2.", theMessage );
		assertEquals( "Exception. Also notable: Suppressed 1. Caused by: Cause 1. Suppressed 2. Caused by: Cause A. Also notable: Suppressed A1. Caused by: Cause A1. Suppressed A2. Caused by: Cause B. Also notable: Suppressed B1. Caused by: Cause B1. Suppressed B2.", theException.toMessage( MessageDetails.FULL ) );
	}

	@Test
	public void testToLocalizedMessage() {
		final BugException theException = createException();
		final String theMessage = theException.toLocalizedMessage();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "toLocalizedMessage() = " + theMessage );
		}
		assertEquals( "Exception. Also notable: Suppressed 1. Caused by: Cause 1. Suppressed 2. Caused by: Cause A. Also notable: Suppressed A1. Caused by: Cause A1. Suppressed A2. Caused by: Cause B. Also notable: Suppressed B1. Caused by: Cause B1. Suppressed B2.", theMessage );
		assertEquals( "Exception. Also notable: Suppressed 1. Caused by: Cause 1. Suppressed 2. Caused by: Cause A. Also notable: Suppressed A1. Caused by: Cause A1. Suppressed A2. Caused by: Cause B. Also notable: Suppressed B1. Caused by: Cause B1. Suppressed B2.", theException.toMessage( MessageDetails.FULL_LOCALIZED ) );
	}

	@Test
	public void testToShortMessage() {
		final BugException theException = createException();
		final String theMessage = theException.toShortMessage();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "toShortMessage() = " + theMessage );
		}
		assertEquals( "Cause 1. Suppressed 2. Caused by: Cause A1. Suppressed A2. Caused by: Cause B. Also notable: Cause B1. Suppressed B2.", theMessage );
		assertEquals( "Cause 1. Suppressed 2. Caused by: Cause A1. Suppressed A2. Caused by: Cause B. Also notable: Cause B1. Suppressed B2.", theException.toMessage( MessageDetails.SHORT ) );
	}

	@Test
	public void testToShortLocalizedMessage() {
		final BugException theException = createException();
		final String theMessage = theException.toShortLocalizedMessage();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "toShortLocalizedMessage() = " + theMessage );
		}
		assertEquals( "Cause 1. Suppressed 2. Caused by: Cause A1. Suppressed A2. Caused by: Cause B. Also notable: Cause B1. Suppressed B2.", theMessage );
		assertEquals( "Cause 1. Suppressed 2. Caused by: Cause A1. Suppressed A2. Caused by: Cause B. Also notable: Cause B1. Suppressed B2.", theException.toMessage( MessageDetails.SHORT_LOCALIZED ) );
	}

	@Test
	public void testFormatMessage1() {
		final String theText = "I love the fruits {0}!";
		final String[] theFruits = new String[] { "apple", "banana", "orange" };
		final String theResult = Trap.asMessage( theText, new Object[] { theFruits } );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "I love the fruits [\"apple\", \"banana\", \"orange\"]!", theResult );
	}

	@Test
	public void testFormatMessage2() {
		final String theText = "I love the fruits {0} and the color {1}!";
		final String[] theFruits = new String[] { "apple", "banana", "orange" };
		final String theResult = Trap.asMessage( theText, new Object[] { theFruits, "blue" } );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "I love the fruits [\"apple\", \"banana\", \"orange\"] and the color blue!", theResult );
	}

	@Test
	public void testFormatMessage3() {
		final String theText = "I love the fruits {0} and the color {1}!";
		final String[] theFruits = new String[] {};
		final String theResult = Trap.asMessage( theText, new Object[] { theFruits, "blue" } );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "I love the fruits {0} and the color blue!", theResult );
	}

	@Test
	public void testFormatMessage4() {
		final String theText = "I love the fruits {0} and the color {1}!";
		final String[] theFruits = new String[] { "apple", null, "orange" };
		final String theResult = Trap.asMessage( theText, new Object[] { theFruits, "blue" } );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "I love the fruits [\"apple\", null, \"orange\"] and the color blue!", theResult );
	}

	@Test
	public void testFormatMessage5() {
		final String theText = "I love the fruits {0} and the color {1}!";
		final String[] theFruits = null;
		final String theResult = Trap.asMessage( theText, new Object[] { theFruits, "blue" } );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "I love the fruits null and the color blue!", theResult );
	}

	@Test
	public void testFormatMessage6() {
		final String theText = "I love the fruits {0!";
		final String[] theFruits = new String[] { "apple", "banana", "orange" };
		final String theResult = Trap.asMessage( theText, new Object[] { theFruits } );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( "I love the fruits {0! ([\"apple\", \"banana\", \"orange\"])", theResult );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private BugException createException() {
		final BugException theCauseB = new BugException( "Cause B" );
		final BugException theSuppressedB1 = new BugException( "Suppressed B1", new BugException( "Cause B1" ) );
		final BugException theSuppressedB2 = new BugException( "Suppressed B2" );
		theCauseB.addSuppressed( theSuppressedB1 );
		theCauseB.addSuppressed( theSuppressedB2 );
		final BugException theCauseA = new BugException( "Cause A", theCauseB );
		final BugException theSuppressedA1 = new BugException( "Suppressed A1", new BugException( "Cause A1" ) );
		final BugException theSuppressedA2 = new BugException( "Suppressed A2" );
		theCauseA.addSuppressed( theSuppressedA1 );
		theCauseA.addSuppressed( theSuppressedA2 );
		final BugException theException = new BugException( "Exception", theCauseA );
		final BugException theSuppressed1 = new BugException( "Suppressed 1", new BugException( "Cause 1" ) );
		final BugException theSuppressed2 = new BugException( "Suppressed 2" );
		theException.addSuppressed( theSuppressed1 );
		theException.addSuppressed( theSuppressed2 );
		return theException;
	}
}
